import React from "react";
import { Player } from "video-react";
import "./videoItem.css";
import "./video-react.scss"; // import css
import { Typography } from "antd";
import { Comment, Tooltip, List, Avatar } from "antd";
import moment from "moment";
import { Link } from "react-router-dom";

export default props => {
  const { Text } = Typography;

  const data = [
    {
      actions: [<span>Reply to</span>],
      author: "Han Solo",
      avatar:
        "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
      content: (
        <p>
          We supply a series of design principles, practical patterns and high
          quality design resources (Sketch and Axure), to help people create
          their product prototypes beautifully and efficiently.
        </p>
      ),
      datetime: (
        <Tooltip
          title={moment()
            .subtract(1, "days")
            .format("YYYY-MM-DD HH:mm:ss")}
        >
          <span>
            {moment()
              .subtract(1, "days")
              .fromNow()}
          </span>
        </Tooltip>
      )
    },
    {
      actions: [<span>Reply to</span>],
      author: "Han Solo",
      avatar:
        "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
      content: (
        <p>
          We supply a series of design principles, practical patterns and high
          quality design resources (Sketch and Axure), to help people create
          their product prototypes beautifully and efficiently.
        </p>
      ),
      datetime: (
        <Tooltip
          title={moment()
            .subtract(2, "days")
            .format("YYYY-MM-DD HH:mm:ss")}
        >
          <span>
            {moment()
              .subtract(2, "days")
              .fromNow()}
          </span>
        </Tooltip>
      )
    }
  ];
  const ExampleComment = ({ children }) => (
    <Comment
      actions={[<span>Reply to</span>]}
      author={
        <a>
          <Link to="/profile/bing107">Hassan Umair</Link>
        </a>
      }
      avatar={
        <Avatar
          src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          alt="Han Solo"
        />
      }
      content={
        <p>
          We supply a series of design principles, practical patterns and high
          quality design resources (Sketch and Axure).
        </p>
      }
    >
      {children}
    </Comment>
  );

  return (
    <div>
      <Player>
        <source src={require("./components/video.mp4")} />
      </Player>
      <br />
      <br />
      <h3>
        <Link to="/videobyid">
          Trailer - New Upcoming Hollywood Animated Movie (2019)
        </Link>
      </h3>
      <br />
      <br />
      <Text>Uploaded by</Text>
      <br />

      <Text strong>
        <Link to="/profile/Najib%20Shah">Najib Shah</Link>
      </Text>

      <br />
      <br />
      <Text>Description</Text>
      <br />
      <Text strong>
        Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
        ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam
        porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac
        cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo
        sit amet risus
      </Text>
      <br />
      <br />
      <Text>Comments</Text>

      <br />
      <ExampleComment>
        <ExampleComment>
          <ExampleComment />
          <ExampleComment />
        </ExampleComment>
      </ExampleComment>
      <br />
      <br />
      <br />
      <br />
    </div>
  );
};
