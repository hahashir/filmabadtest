import React, { Component } from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";

class compLanding extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-7 m-auto">
            <br />
            <br />
            <Button
              style={{ marginRight: "20px" }}
              type="primary"
              size={"large"}
            >
              <Link to="/newComp">New Competition</Link>
            </Button>

            <Button
              style={{ marginRight: "20px" }}
              type="primary"
              size={"large"}
            >
              <Link to="/activecomp">Active Competitions</Link>
            </Button>

            <Button type="primary" size={"large"}>
              <Link to="/resultscomp">Competition Results</Link>
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default compLanding;
