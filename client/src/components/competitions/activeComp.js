import React, { Component } from "react";
import { Carousel } from "antd";
import { Link } from "react-router-dom";

import "antd/dist/antd.css";
import "./activeComp.css";
import { Col, Row } from "antd";
import { Card } from "antd";
import { Skeleton, Icon, Avatar } from "antd";

class activeComp extends Component {
  render() {
    const { Meta } = Card;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12 m-auto">
            <h2>Active Competitions</h2>
            <Carousel autoplay>
              <div>
                <h3>Take Part In Competitions</h3>
              </div>
              <div>
                <h3>Win Prizes</h3>
              </div>
              <div>
                <h3>Get Highlighted</h3>
              </div>
              <div>
                <h3>Gain More Audience</h3>
              </div>
              <br />
            </Carousel>
            <br />
            <div style={{ background: "#ECECEC", padding: "30px" }}>
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />
              <Row gutter={16}>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
                <Col span={8}>
                  <Card
                    style={{ width: 300 }}
                    cover={
                      <Link to="/onecomp">
                        <img
                          alt="example"
                          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                        />
                      </Link>
                    }
                    actions={[
                      <Icon type="upload" />,
                      <Icon type="star" />,
                      <Icon type="ellipsis" />
                    ]}
                  >
                    <Meta
                      avatar={
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      }
                      title="Competition title"
                      description="This is the description"
                    />
                  </Card>
                </Col>
              </Row>
              <br />

              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default activeComp;
