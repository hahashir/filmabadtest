import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Upload, Icon, message } from "antd";
import "antd/dist/antd.css";
import "./newVideo.css";

import { Typography } from "antd";
import { Select } from "antd";
import { Button } from "antd";
import { Input } from "antd";

function handleChange(value) {
  console.log(`selected ${value}`);
}

class newVideo extends Component {
  render() {
    const { Title } = Typography;
    const { Text } = Typography;
    const Dragger = Upload.Dragger;
    const Option = Select.Option;
    const { TextArea } = Input;

    const props = {
      name: "file",
      multiple: true,
      action: "//jsonplaceholder.typicode.com/posts/",
      onChange(info) {
        const status = info.file.status;
        if (status !== "uploading") {
          console.log(info.file, info.fileList);
        }
        if (status === "done") {
          message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === "error") {
          message.error(`${info.file.name} file upload failed.`);
        }
      }
    };

    const children = [];

    children.push(<Option value="action">Action</Option>);
    children.push(<Option value="fiction">Fiction</Option>);
    children.push(<Option value="scifi">Sci-Fi</Option>);
    children.push(<Option value="thriller">Thriller</Option>);
    children.push(<Option value="adventure">Adventure</Option>);
    children.push(<Option value="crime">Crime</Option>);
    children.push(<Option value="comedy">Comedy</Option>);
    children.push(<Option value="fantasy">fantasy</Option>);
    children.push(<Option value="horror">horror</Option>);
    children.push(<Option value="romance">romance</Option>);
    children.push(<Option value="satire">satire</Option>);
    children.push(<Option value="animation">animation</Option>);
    children.push(<Option value="musicvideo">music video</Option>);
    children.push(<Option value="comedy">Comedy</Option>);

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <br />
            <br />
            <Title mark className="upload-video-text">
              Upload Your Video
            </Title>
            <Dragger {...props}>
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>

              <p className="ant-upload-text">
                Click or drag file to this area to upload
              </p>
              <p className="ant-upload-hint">
                Please make sure you are located in Pakistan. This site only
                supports Pakistani Content.
              </p>
            </Dragger>
            <br />
            <br />
          </div>
        </div>
        <div className="col-md-6 m-auto">
          <Text strong>Description</Text>

          <br />
          <TextArea
            placeholder="Tell us something about your film like the concept or a detailed description"
            autosize={{ minRows: 5, maxRows: 12 }}
          />
          <br />
          <br />

          <Text strong>Select Genres</Text>
          <br />
          <div className="select">
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              placeholder="Please select"
              defaultValue={["Action", "Romantic"]}
              onChange={handleChange}
            >
              {children}
            </Select>
          </div>
          <br />

          <Text strong>Add Tags</Text>
          <div className="select">
            <Select
              mode="tags"
              style={{ width: "100%" }}
              placeholder="Tags Mode"
              onChange={handleChange}
            >
              {children}
            </Select>
            <br />
            <br />
            <br />
          </div>
          <Link to="/videobyid">
            <Button type="primary">Finish Upload</Button>
          </Link>
        </div>
      </div>
    );
  }
}

export default newVideo;
